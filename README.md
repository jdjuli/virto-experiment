Package to reproduce the experiment
===================================

Here you'll find all the resources needed to replicate the experiment

Sample programs
---------------

Below there are two links to view a simple program with an sequential program and a complex one with conditionals and loops. This two scenes are explained on the paper.

*   [Simple scene](./scene/example/simple)
*   [Complex scene](./scene/example/complex)

---

Experiment
----------

First we'll let the users to get used to the environment throught three train scenes with an increasing difficulty; the first features a simple sequential program, the second, a program with the two available control structures (while-loop and conditional), and the last one is completely empty to make sure the participant knows how to create an arbitrary program. Deliberately, the train scenes don't show up the posibility of control structures nesting (i.e. A loop inside a branch of a condition) which is required on the test task, to check if the participant has inferred that such thing is possible on this languaje.

Secondly, after finishing the train scenes, we'll show the participant a description and a non-technical diagram of a program and ask them to implement it themselves. This part of the experiment will be timed to have a quantitative measurement of how much time is required to implement a given project after a brief training

The experiment concludes with an interview on which, apart from some basic demographic questions, we'll ask others about the participant impression about this tool

---

#### First part

| Step    | Description                          |  Time  |
| ------- | ------------------------------------ | -------|
| Train 1 | Maniputation of a sequential program | 5 min  |
| Train 2 | Introduction to control instructions | 5 min  |
| Train 3 | Creation of an arbitrary program     | 5 min  |
| Test    | Implementation of a given program    | 20 min |
*The whole experiment will be recorded with the Meta Quest 2 without capturing any kind of audio

---

#### Notes for the experiment instructors

*   The usage of the VR scenes of the experiment has to be recorded without voice to analyze afterwards possible common difficulties among participants not reflected on the interview
*   To perform the experiment, a Meta Quest 2 VR headset is needed

*   It's navigator has to have the experimental flag 'WebXR user activation requirement' enabled, to do so, open the url 'chrome://flags', find the flag and set it to 'Disabled' ([check out this image for guidance](resources/imgs/flagToBeDisabled.jpg))

*   The participant only has to open the Train scene link, all the scenes except the test one have a button on the left that makes them advance to the next scene
*   Remind the participant to let you know when they are going to start the test (right before they start to program, not when they start to read the task for the first time) and when they finish it

---

#### Links and documents

##### Train

[Train scene](scene/train/step1)

##### Test

[Program description](resources/docs/ProgramDescription.pdf) [Test scene](scene/test)

##### Interview

[Questionary](resources/docs/Questionary.pdf)

  

---

Experiment results
------------------

In the folder 'results' there is a .CSV file called 'results.csv' with the data adquired throught the experiment, along with the graphics used on the paper.

The description of the headers present on the .CSV are available on the [Questionary](resources/docs/Questionary.pdf)

Along with the 'results.csv' file, there is also a version of it called 'results\_formatted.csv', on which the times have been manually parsed and it has been used to generate the graphics

---

Attributions
------------

*   textures/variables/\*.png Downloaded from [openmoji.com](https://openmoji.org/) ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en))
*   textures/controller.png Downloaded from [developer.oculus.com](https://developer.oculus.com/downloads/package/oculus-controller-art/) ([Art Attribution License 1.0](https://developer.oculus.com/licenses/art-1.0))
*   textures/Wood027\_ambientCG.png Downloaded from [ambientCG.com](https://ambientCG.com/a/Wood027) ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/))
*   models/drone.gltf Created by Egor Likhachev and downloaded from [sketchfab.com](https://skfb.ly/otPA9) ([CC BY 1.0](https://creativecommons.org/licenses/by/4.0/))

Any other file not listed above has been created by the owner of this repository and is distributed under the [MIT License](https://choosealicense.com/licenses/mit/)  
  

---

Other resources
---------------

In addition to the repository and the files provided, here are the links to the [development repository](https://github.com/jdjuli/virto) and the [blog](https://jdjuli.github.io/virto) written during ViRto development.